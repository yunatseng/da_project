# 試題一

## Run the System

```bash
docker-compose up
```

Docker will pull the MongoDB and Node.js images (if your machine does not have it before).

The services can be run on the background with command:

```bash
docker-compose up -d
```

## Stop the System

Stopping all the running containers is also simple with a single command:

```bash
docker-compose down
```

If you need to stop and remove all containers, networks, and all images used by any service in <em>docker-compose.yml</em> file, use the command:

```bash
docker-compose down --rmi all
```

## 查看爬下來的資料

可連進開在 `port:7017` 的 mongoDB，即可查看在 `my_project` DB 下 licenses collection 中的資料。
