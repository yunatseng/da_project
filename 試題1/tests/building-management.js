// Playwright 設置
const { chromium } = require('playwright')
const { request } = require('@playwright/test')
const Tesseract = require('tesseract.js')

// DB 設置
const { MongoClient } = require("mongodb");
const uri = "mongodb://mongodb:27017";
// const uri = "mongodb://localhost:27017";
const dbName = 'my_project';

// Create a new MongoClient
const client = new MongoClient(uri);

// 爬蟲開始
const BASE_URL =
  'https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp'

const launchOptions = {
  headless: true,
  // headless: false,
}

async function run() {
  try {
    // Connect the client to the server (optional starting in v4.7)
    await client.connect();
    // Establish and verify connection
    const licenses = client.db(dbName).collection('licenses');

    const browser = await chromium.launch(launchOptions)
    const context = await browser.newContext()
    const page = await context.newPage()

    await page.goto(BASE_URL)

    // TODO: 這裡用 funciton 包起來跑迴圈
    // Click 關閉提醒視窗
    await page.locator('text=關閉').click()

    // 截圖驗證碼轉成 base64 Buffer
    const buffer = await page.locator('#codeimg').screenshot()
    const base64Img = buffer.toString('base64')
    const base64String = 'data:image/png;base64,' + base64Img
    // 傳入套件開始驗證
    const {
      data: { text },
    } = await Tesseract.recognize(
      Buffer.from(base64String.split(',')[1], 'base64'),
      'eng'
    ).catch((error) => console.log(error))
    //   console.log(text)

    // Click input[name="A2V"] 選取執照
    const [page1] = await Promise.all([
      page.waitForEvent('popup'),
      page.locator('input[name="A2V"]').click(),
    ])
    await page1.waitForLoadState('load');
    await page1.locator('text=使用執照 >> nth=1').click()

    // Click input[name="D1V"] 選取區域
    const [page2] = await Promise.all([
      page.waitForEvent('popup'),
      page.locator('input[name="D1V"]').click(),
    ])

    // 先抓有幾個區
    await page2.waitForLoadState('load');
    const countyNum = (await page2.$$('div')).length
    console.log(countyNum)
    // 抓區的文字
    const countyText = await page2.locator('div').nth(0).textContent()
    const [county, town] = countyText.split('市')

    // 這邊之後要改成 i
    await page2.locator('.div_w_85 >> nth=0').click()

    // 開一個 request 實例準備 call API
    const fetchRoadData = await request.newContext()

    // call 道路名 API
    const responseData = await fetchRoadData
      .get(
        `https://www.ris.gov.tw/rs-opendata/api/v1/datastore/ODRP049/111?COUNTY=%E6%96%B0%E5%8C%97%E5%B8%82&TOWN=${town}`
      )
      .then((response) => response.json())
      .then((res) => res.responseData)
      .catch((error) => console.log(error))

    // 拿到路名資料
    const road = responseData[0].road

    // Fill input[name="D3"] 路名
    await page.locator('#D3').fill(road)

    // Fill input[name="Z1"] 填寫驗證碼
    await page.locator('#Z1').fill(text)

    // Click button:has-text("查詢")
    await Promise.all([
      page.waitForNavigation(/*{ url: 'https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp' }*/),
      page.locator('button:has-text("查詢")').click()
    ]);

    const tableNum = (await page.$$('#DataBlock > tr')).length
    const totalNum = await page.locator('#RecCT').textContent()
    const totalPage = Math.ceil(totalNum / tableNum)
    const tableData = []

    for (let index = 0; index < totalPage; index++) {
      console.log(`抓取第 ${index + 1} 頁`);

      await page.waitForSelector('#DataBlock', '#bt_reset');

      const rowCount = (await page.$$('#DataBlock > tr')).length
      console.log(`總共有 ${rowCount} 列`);

      for (let i = 0; i < rowCount; i++) {
        console.log(`抓取第 ${index + 1} 頁，第 ${i + 1} 列`);

        const userLicense = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(0).textContent()
        const buildingLicense = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(1).textContent()
        const builder = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(2).textContent()
        const designer = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(3).textContent()
        const location = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(4).textContent()
        const permitDate = await page.locator('#DataBlock > tr').nth(i).locator('td').nth(5).textContent()

        const doc = { '使照號': userLicense, '建照號': buildingLicense, '起造人': builder, '設計人': designer, '建築地址': location, '發照日': permitDate };
 
        tableData.push(doc)
      }

      await page.locator('text=下一頁').click()
    }
    await licenses.insertMany(tableData);
    await page.locator('#bt_reset').click()
  } finally {
    await client.close();
  }
}

run().catch((error) => {
  console.dir(error);
  process.exit(1);
});